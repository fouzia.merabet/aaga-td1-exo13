#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BIT 1 
#define OCTET 8*BIT
#define KO OCTET*1024
#define MO KO*1024 
#define P1 1.0

unsigned char int_to_bin(int* tab) {
  
  unsigned char sum = 0;
  int coeff_pow = 7;
  
  for (int i=0; i<OCTET; i++, coeff_pow--) {
    sum += (tab[i] << coeff_pow);
  }
  
  return sum;
}

double rand_0_1() {
  return (double)rand() / (double)RAND_MAX;
}

int main() {
  
  //Initialiser le générateur
  srand(time(NULL));
	
  FILE* f = fopen("compression_1_0.data", "wb");
  
  if (!f) {
    fprintf(stderr, "Impossible d'ouvrir le fichier");
    exit(EXIT_FAILURE);
  }
  
  unsigned char buffer[1];
  int bits[OCTET];
  int i=0;
  
  while (i<MO) {
    
    //Remplir le buffer avec 8 bits
    for (int j=0; j<OCTET; j++) {
      if (rand_0_1() < P1) {
        bits[j] = 1;
      } else {
        bits[j] = 0;
      }
    }
    
    buffer[0] = int_to_bin(bits);
    
    //Stocker le buffer dans le fichier
    fwrite(buffer, sizeof(unsigned char), 1, f);

    i += OCTET;
  }
  
  fclose(f);  
	
	return 0;
}
